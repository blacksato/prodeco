<?php

namespace App\Listeners;

use App\Events\EmployeeEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmployeeEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EmployeeEvent  $event
     * @return void
     */
    public function handle(EmployeeEvent $event)
    {
        $objEmployee=$event->employee;
        $objEmployee->ages=$objEmployee->birthDate;
        $objEmployee->user_created=\Auth::user()->id;
        $objEmployee->user_updated=\Auth::user()->id;

    }
}
