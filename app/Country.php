<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{

    public function employees(){
        return $this->hasMany(Employee::class,'nationality_id','id');
    }
}
