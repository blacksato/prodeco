<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocEmployee extends Model
{
    protected $table="doc_employees";
    public function employee(){
        return $this->belongsTo(Employee::class,'employee_id','id');
    }
}
