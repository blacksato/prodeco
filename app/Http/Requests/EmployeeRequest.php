<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Input;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id=null;
        $id=$this->employee==null?null:$this->employee->id;

        $rules= [
            'name'=>'required|names',
            'lastName'=>'required|names',
            'birthDate'=>'required|date',
            'nationality_id'=>'required|numeric',
            'type_id'=>'required|in:nic,passport',
            'nic'=>"required|alpha_num|unique:employees,nic,{$id},id,deleted_at,NULL",
            'address'=>'required',
            'email'=>'required|email',
            'type_blood'=>'required|in:A+,A-,B+,B-,AB+,AB-,O+,O-',
            'phoneNumber'=>'required|numeric',
            'phoneReferences'=>'required|numeric',
            'permissionJob'=>'required|in:S,N'
        ];

        return $rules;
    }
}
