<?php

namespace App\Http\Controllers;

use App\Country;
use App\DocEmployee;
use App\Employee;

use App\Http\Requests\EmployeeRequest;
use Illuminate\Http\Request;
use Facades\App\Facades\AlertManager;

use DB;
use File;
use Storage;
class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $employees = Employee::with('docemployees')->with('nationality')->where('name', 'like', "%$request->scope%")
            ->orWhere('nic', 'like', "%$request->scope%")
            ->orderBy('lastName', 'asc')->paginate(1);
        return view('employees.index')->with(['employees' => $employees, 'scope' => $request->scope]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $objNationality = Country::pluck('nationality', 'id');
        return view('employees.create')->with(['objNationality' => $objNationality]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {

        try {
           DB::transaction(function ()use($request) {
                $objEmployee = new Employee();
                $objEmployee->fill($request->all());
                $objEmployee->save();
                $photo = $request->file('filePhoto');
                if ($photo != null) {
                    $namefile = 'PHOTO'.$request->nic . "." . $photo->getClientOriginalExtension();
                    Storage::disk('employees')->delete($namefile);
                    Storage::disk('employees')->put($namefile, File::get($photo));
                    $objDocEmployee =new DocEmployee();
                    $objDocEmployee->employee()->associate($objEmployee);
                    $objDocEmployee->type='PHOTO';
                    $objDocEmployee->filename=$namefile;
                    $objDocEmployee->save();
                }

               $filePermissionJob = $request->file('filePermissionJob');
               if ($filePermissionJob != null) {
                   $namefile = 'PERMISSION'.$request->nic . "." . $filePermissionJob->getClientOriginalExtension();
                   Storage::disk('employees')->delete($namefile);
                   Storage::disk('employees')->put($namefile, File::get($filePermissionJob));
                   $objDocEmployee =new DocEmployee();
                   $objDocEmployee->employee()->associate($objEmployee);
                   $objDocEmployee->type='PERMISSION';
                   $objDocEmployee->filename=$namefile;
                   $objDocEmployee->save();
               }

               $docExtras=$request->file('docExtras');
               if($docExtras!=null){
                   foreach ($docExtras as $doc){
                       $namefile = 'DOCEXTRA'.$request->nic . time()."." . $doc->getClientOriginalExtension();
                       Storage::disk('employees')->put($namefile, File::get($doc));
                       $objDocEmployee =new DocEmployee();
                       $objDocEmployee->employee()->associate($objEmployee);
                       $objDocEmployee->type='DOCEXTRA';
                       $objDocEmployee->filename=$namefile;
                       $objDocEmployee->save();
                   }
               }





               AlertManager::success('Registro guardado correctamente');
            });

            return redirect()->route('employees.index');
        } catch (\Exception $ex) {
            AlertManager::danger('Error de proceso: ' . $ex->getMessage());
            return redirect()->back()->withInput();
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {

        return view('employees.show')->with(['employee'=>$employee]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        $objNationality = Country::pluck('nationality', 'id');
        return view('employees.edit')->with(['objNationality' => $objNationality,'employee'=>$employee]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeRequest $request, Employee $employee)
    {
        try {
            DB::transaction(function ()use($request,$employee) {
                $employee->fill($request->all());
                $employee->save();
                $photo = $request->file('filePhoto');
                if ($photo != null) {
                    $namefile = 'PHOTO'.$request->nic . "." . $photo->getClientOriginalExtension();
                    Storage::disk('employees')->delete($namefile);
                    Storage::disk('employees')->put($namefile, File::get($photo));

                    $objDocEmployee =DocEmployee::where('type','=','PHOTO')->where('employee_id','=',$employee->id)->first();

                    if($objDocEmployee==null){
                        $objDocEmployee=new DocEmployee();

                    }
                    $objDocEmployee->employee()->associate($employee);
                    $objDocEmployee->type='PHOTO';
                    $objDocEmployee->filename=$namefile;
                    $objDocEmployee->save();
                }

                $filePermissionJob = $request->file('filePermissionJob');
                if ($filePermissionJob != null) {
                    $namefile = 'PERMISSION'.$request->nic . "." . $filePermissionJob->getClientOriginalExtension();
                    Storage::disk('employees')->delete($namefile);
                    Storage::disk('employees')->put($namefile, File::get($filePermissionJob));

                    $objDocEmployee =DocEmployee::where('type','=','PERMISSION')->where('employee_id','=',$employee->id)->first();

                    if($objDocEmployee==null){
                        $objDocEmployee=new DocEmployee();

                    }
                    $objDocEmployee->employee()->associate($employee);
                    $objDocEmployee->type='PERMISSION';
                    $objDocEmployee->filename=$namefile;
                    $objDocEmployee->save();
                }

                $docExtras=$request->file('docExtras');

                if($docExtras!=null){
                    DocEmployee::where('type','=','DOCEXTRA')->where('employee_id','=',$employee->id)->delete();
                    foreach ($docExtras as $doc){
                        $namefile = 'DOCEXTRA'.$request->nic . time()."." . $doc->getClientOriginalExtension();
                        Storage::disk('employees')->delete($namefile);
                        Storage::disk('employees')->put($namefile, File::get($doc));




                            $objDocEmployee=new DocEmployee();


                        $objDocEmployee->employee()->associate($employee);
                        $objDocEmployee->type='DOCEXTRA';
                        $objDocEmployee->filename=$namefile;
                        $objDocEmployee->save();
                    }
                }








                AlertManager::success('Registro editado correctamente');
            });

            return redirect()->route('employees.index');
        } catch (\Exception $ex) {
            AlertManager::danger('Error de proceso: ' . $ex->getMessage());
            return redirect()->back()->withInput();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        //
    }
}
