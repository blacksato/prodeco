<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table="employees";
    protected $fillable=[ 'name','lastName',
        'birthDate',
        'nationality_id',
        'ages',
        'type_id',
        'nic',
        'address',
        'email',
        'type_blood',
        'phoneNumber',
        'phoneReferences',
        'permissionJob'];

    protected $events=[
        'creating' => Events\EmployeeEvent::class,
        'updating' => Events\EmployeeEvent::class,
    ];


    public function setAgesAttribute($value)
    {
        $this->attributes['ages'] = Carbon::parse($value)->age;
    }

    public function nationality(){
        return $this->belongsTo(Country::class,'nationality_id','id');
    }
    public function docemployees(){
        return $this->hasMany(DocEmployee::class,'employee_id','id');
    }

}
