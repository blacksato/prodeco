<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Schema;
use Validator;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Validator::extend('names', function($attribute, $value) { return preg_match('/^[a-zñáéíóúA-ZÑÁÉÍÓÚ ]*$/', $value); });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
