<?php

return [

    'TypeBlood' => [
        'A+' => 'A+',
        'A-' => 'A-',
        'B+' => 'B+',
        'B-' => 'B-',
        'AB+' => 'AB+',
        'AB-' => 'AB-',
        'O+' => 'O+',
        'O-' => 'O-'
    ],

    'TypeNic'=>[
        'nic'=>'NIC',
        'passport'=>'PASAPORTE'
    ],

    'afirmation'=>[
        'N'=>'NO',
        'S'=>'SI'

    ]
];