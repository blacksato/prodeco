<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Employees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('lastName');
            $table->integer('ages');
            $table->date('birthDate');
            $table->integer('nationality_id')->unsigned();
            $table->enum('type_id', ['nic','passport']);
            $table->string('nic')->unique();
            $table->string('address');
            $table->string('email');
            $table->string('type_blood');
            $table->integer('user_created')->unsigned();
            $table->integer('user_updated')->unsigned();
            $table->integer('user_deleted')->nullable()->unsigned();
            $table->softDeletes();
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
