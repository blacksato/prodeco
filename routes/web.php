<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index');


Route::group(['middleware' => ['auth']],function(){
Route::resource('employees','EmployeeController');

Route::get('employees-photo/{file}', function ($file = null) {
    $url = storage_path() . "/app/public/employees/{$file}";
    if (file_exists($url)) {
        return Response::download($url);
    }
});
});