<div class="row clearfix" style="font-size: 12px !important;">
    <div class="col-lg-3 col-md-3 form-control-label">
        <label for="{{$name}}" style="text-transform: uppercase;    padding: 0px;">{{ $title }}</label>
    </div>
    <div class="col-lg-9 col-md-9" style="margin-bottom: 2px">
        <div class="form-group">
            <div class="form-line">
                @if($readonly)
               {!! Form::text($name,$value,['id'=>$name,'class'=>"form-control $slot",'readonly'=>'readonly']) !!}
                @else
                {!! Form::text($name,$value,['id'=>$name,'class'=>"form-control $slot"]) !!}
                @endif

            </div>
        </div>
    </div>
</div>