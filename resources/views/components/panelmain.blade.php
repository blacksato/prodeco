<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">
    <div class="card">

        <div class="header bg-cyan">
            <h2>
               {{$title}}
            </h2>
        </div>
        <div class="body">
            @include('components.alertmanager')
                {{$body}}
        </div>
    </div>
</div>