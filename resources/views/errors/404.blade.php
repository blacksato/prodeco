<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>404 | Prodeco</title>
    <!-- Favicon-->
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/template/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/template/waves.css') }}" rel="stylesheet">
    <link href="{{ asset('css/template/style.css') }}" rel="stylesheet">
</head>

<body class="four-zero-four">
<div class="four-zero-four-container">
    <div class="error-code">404</div>
    <div class="error-message">P&aacute;gina no encontrada</div>
    <div class="button-place">
        <a href="/home" class="btn btn-default btn-lg waves-effect">INICIO</a>
    </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>


<script src="{{ asset('js/template/waves.js') }}"></script>
<script src="{{ asset('js/template/admin.js') }}"></script>
</body>

</html>