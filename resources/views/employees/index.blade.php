@extends('layouts.app')
@section('titleContent','M&oacute;dulo de Empleados')

@section('barrabusqueda')
    {!! Form::open(['route'=>'employees.index','method'=>'GET'])!!}
        {!! Form::text('scope',$scope,['placeholder'=>"Búsqueda de Empleados"]) !!}
    {!! Form::close()!!}
    
@endsection


@section('content')

    @component('components.panelmain')
    @slot('title','Listado de Empleados')
    @slot('body')

    <div class="body">
        <div class="row">
            <div class="col-md-12 text-center">
                <a  href="{{ route('employees.create') }}" class="btn bg-teal waves-effect"><i class="material-icons">add_box</i> CREAR</a>
            </div>

        </div>

            {!! $employees->render() !!}
            <table style="width: 100%">
            <tbody>
            @foreach($employees as $employee)
            <tr>
                <td>
                    <div class="card panel panel-primary panel-col-cyan">
                        <div class="panel-body">
                            <div class="col-md-3 text-center">
                                <?php
                                    $photo = $employee->docemployees->filter(function($item) {
                                        return $item->type == 'PHOTO';
                                    })->first();

                                    $photoName=$photo==null?'/image/logo.png':'/employees-photo/'.$photo->filename;
                                ?>

                                <img src="{{$photoName}}" class="img-thumbnail" width="120px" height="120px">
                                <div class="row">
                                    <a title="EDITAR" href="{{ route('employees.edit',$employee->id) }}"><i class="material-icons">mode_edit</i></a>
                                    <a title="VER" href="{{ route('employees.show',$employee->id) }}"><i class="material-icons text-success">chrome_reader_mode</i></a>

                                </div>
                            </div>
                            <div class="col-md-9 form-horizontal">
                                @component('components.item')
                                @slot('name','name')
                                @slot('title','NOMBRES: ')
                                @slot('value',$employee->name.' '.$employee->lastName)
                                @slot('readonly',true)
                                @endcomponent

                                @component('components.item')
                                @slot('name','birthDate')
                                @slot('title','FECHA DE NAC: ')
                                @slot('value',$employee->birthDate)
                                @slot('readonly',false)
                                @endcomponent

                                @component('components.item')
                                @slot('name','nationality_id')
                                @slot('title','Nacionalidad: ')
                                @slot('value',$employee->nationality->nationality)
                                @slot('readonly',false)
                                @endcomponent

                                @component('components.item')
                                @slot('name','nic')
                                @slot('title','N&Uacute;MERO IDENTIFICACI&Oacute;N: ')
                                @slot('value',$employee->nic)
                                @slot('readonly',false)
                                @endcomponent

                                @component('components.item')
                                @slot('name','email')
                                @slot('title','E-mail: ')
                                @slot('value',$employee->email)
                                @slot('readonly',false)
                                @endcomponent



                                @component('components.item')
                                @slot('name','phoneNumber')
                                @slot('title','#Telf: ')
                                @slot('value',$employee->phoneNumber)
                                @slot('readonly',false)
                                @endcomponent










                            </div>

                        </div>
                    </div>


                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    @endslot
    @endcomponent

@endsection
@section('jsCustom')
    <script>
        $(function () {
            $("div").removeClass('focused');
        });

    </script>
@endsection