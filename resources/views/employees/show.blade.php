@extends('layouts.app')
@section('titleContent','M&oacute;dulo de Empleados')
@section('barrabusqueda')
    {!! Form::open(['route'=>'employees.index','method'=>'GET'])!!}
    {!! Form::text('scope','',['placeholder'=>"Búsqueda de Empleados"]) !!}
    {!! Form::close()!!}

@endsection

@section('content')


    @component('components.panelmain')
    @slot('title','Vista de Empleados')
    @slot('body')

    <div class="body">
                   <div class="card panel panel-primary panel-col-cyan">
                        <div class="panel-body">
                            <div class="col-md-12 text-center">
                                <?php
                                $photo = $employee->docemployees->filter(function($item) {
                                    return $item->type == 'PHOTO';
                                })->first();

                                $photoName=$photo==null?'/image/logo.png':'/employees-photo/'.$photo->filename;
                                ?>

                                <img src="{{$photoName}}" class="img-thumbnail" width="120px" height="120px">

                                <br/>


                            </div>

                            <div class="col-md-12 form-horizontal">
                                @component('components.item')
                                @slot('name','name')
                                @slot('title','NOMBRES: ')
                                @slot('value',$employee->name)
                                @slot('readonly',true)
                                @endcomponent

                                @component('components.item')
                                @slot('name','lastName')
                                @slot('title','Apellidos: ')
                                @slot('value',$employee->lastName)
                                @slot('readonly',true)
                                @endcomponent



                                @component('components.item')
                                @slot('name','birthDate')
                                @slot('title','FECHA DE NAC: ')
                                @slot('value',$employee->birthDate)
                                @slot('readonly',true)
                                datepicker
                                @endcomponent




                                @component('components.item')
                                @slot('name','type_id')
                                @slot('title','Nacionalidad: ')
                                @slot('value',$employee->nationality->nationality)
                                @slot('readonly',true)
                                @endcomponent

                                @component('components.item')
                                @slot('name','type_id')
                                @slot('title','TIPO IDENTIFICACI&Oacute;N: ')
                                @slot('value',@config('dataselect.TypeNic')[$employee->type_id])
                                @slot('readonly',true)
                                @endcomponent



                                @component('components.item')
                                @slot('name','nic')
                                @slot('title','N&Uacute;MERO IDENTIFICACI&Oacute;N: ')
                                @slot('value',$employee->nic)
                                @slot('readonly',true)
                                @endcomponent

                                @component('components.item')
                                @slot('name','address')
                                @slot('title','DIRECCI&Oacute;N: ')
                                @slot('value',$employee->address)
                                @slot('readonly',true)
                                @endcomponent


                                @component('components.item')
                                @slot('name','email')
                                @slot('title','E-mail: ')
                                @slot('value',$employee->email)
                                @slot('readonly',true)
                                @endcomponent


                                @component('components.item')
                                @slot('name','type_blood')
                                @slot('title','Tipo De Sangre: ')
                                @slot('value',@config('dataselect.TypeBlood')[$employee->type_blood])
                                @slot('readonly',true)
                                @endcomponent



                                @component('components.item')
                                @slot('name','phoneNumber')
                                @slot('title','#Telf: ')
                                @slot('value',$employee->phoneNumber)
                                @slot('readonly',true)
                                @endcomponent

                                @component('components.item')
                                @slot('name','phoneReferences')
                                @slot('title','Referencia:')
                                @slot('value',$employee->phoneReferences)
                                @slot('readonly',true)
                                @endcomponent



                                @component('components.item')
                                @slot('name','permissionJob')
                                @slot('title','Permiso de trabajo:')
                                @slot('value',@config('dataselect.afirmation')[$employee->permissionJob])
                                @slot('readonly',true)
                                @endcomponent


                                <div class="alert alert-info text-center important">
                                    <strong>Secci&oacute;n de Documentos!</strong> Documentos cargados en el sistema.
                                </div>



                                <ul>
                                    @foreach( $employee->docemployees as $doc)
                                        <li>
                                            <b> {{$doc->type}}: </b>&nbsp; <a href="/employees-photo/{{$doc->filename}}">{{$doc->filename}}</a>
                                        </li>
                                    @endforeach
                                </ul>


                            </div>

                        </div>
                        <div class="panel-footer text-center">
                            <a href="{{route('employees.index')}}" class="btn bg-red waves-effect"><i class="material-icons">keyboard_backspace</i> REGRESAR</a>
                        </div>

                    </div>



    </div>

    @endslot
    @endcomponent

@endsection

@section('jsCustom')
    <script>
        $(function () {
            $("div").removeClass('focused');
        });

    </script>
@endsection