@extends('layouts.app')
@section('titleContent','M&oacute;dulo de Empleados')
@section('barrabusqueda')
    {!! Form::open(['route'=>'employees.index','method'=>'GET'])!!}
    {!! Form::text('scope','',['placeholder'=>"Búsqueda de Empleados"]) !!}
    {!! Form::close()!!}

@endsection

@section('content')


    @component('components.panelmain')
    @slot('title','Editar Empleados')
    @slot('body')

    <div class="body">
        {!! Form::open(['route'=>['employees.update',$employee->id],'method'=>'PUT','enctype'=>'multipart/form-data']) !!}
                    <div class="card panel panel-primary panel-col-cyan">
                        <div class="panel-body">
                            <div class="col-md-12 text-center">
                                <?php
                                $photo = $employee->docemployees->filter(function($item) {
                                    return $item->type == 'PHOTO';
                                })->first();

                                $photoName=$photo==null?'/image/logo.png':'/employees-photo/'.$photo->filename;
                                ?>

                                <img src="{{$photoName}}" class="img-thumbnail" width="120px" height="120px">

                                <br/>


                            </div>



                            <div class="col-md-12 form-horizontal">




                                @component('components.item')
                                @slot('name','name')
                                @slot('title','NOMBRES: ')
                                @slot('value',$employee->name)
                                @slot('readonly',false)
                                @endcomponent

                                @component('components.item')
                                @slot('name','lastName')
                                @slot('title','Apellidos: ')
                                @slot('value',$employee->lastName)
                                @slot('readonly',false)
                                @endcomponent



                                @component('components.item')
                                @slot('name','birthDate')
                                @slot('title','FECHA DE NAC: ')
                                @slot('value',$employee->birthDate)
                                @slot('readonly',false)
                                datepicker
                                @endcomponent



                                @component('components.itemselect')
                                @slot('name','nationality_id')
                                @slot('title','Nacionalidad: ')
                                @slot('content',$objNationality)
                                @slot('default',$employee->nationality_id)
                                @endcomponent


                                @component('components.itemselect')
                                @slot('name','type_id')
                                @slot('title','TIPO IDENTIFICACI&Oacute;N: ')
                                @slot('content',config('dataselect.TypeNic'))
                                @slot('default',$employee->type_id)
                                @endcomponent



                                @component('components.item')
                                @slot('name','nic')
                                @slot('title','N&Uacute;MERO IDENTIFICACI&Oacute;N: ')
                                @slot('value',$employee->nic)
                                @slot('readonly',false)
                                @endcomponent

                                @component('components.item')
                                @slot('name','address')
                                @slot('title','DIRECCI&Oacute;N: ')
                                @slot('value',$employee->address)
                                @slot('readonly',false)
                                @endcomponent


                                @component('components.item')
                                @slot('name','email')
                                @slot('title','E-mail: ')
                                @slot('value',$employee->email)
                                @slot('readonly',false)
                                @endcomponent

                                @component('components.itemselect')
                                @slot('name','type_blood')
                                @slot('title','Tipo De Sangre: ')
                                @slot('content',config('dataselect.TypeBlood'))
                                @slot('default',$employee->type_blood)
                                @endcomponent

                                @component('components.item')
                                @slot('name','phoneNumber')
                                @slot('title','#Telf: ')
                                @slot('value',$employee->phoneNumber)
                                @slot('readonly',false)
                                @endcomponent

                                @component('components.item')
                                @slot('name','phoneReferences')
                                @slot('title','Referencia:')
                                @slot('value',$employee->phoneReferences)
                                @slot('readonly',false)
                                @endcomponent



                                @component('components.itemselect')
                                @slot('name','permissionJob')
                                @slot('title','Permiso de trabajo:')
                                @slot('content',config('dataselect.afirmation'))
                                @slot('default',$employee->permissionJob)
                                @endcomponent


                                <ul>
                                    @foreach( $employee->docemployees as $doc)
                                        <li>
                                            <b> {{$doc->type}}: </b>&nbsp; <a href="/employees-photo/{{$doc->filename}}">{{$doc->filename}}</a>
                                        </li>
                                    @endforeach
                                </ul>


                                <div class="alert alert-info text-center important">
                                    <strong>Secci&oacute;n de Documentos!</strong> Subir los documentos que considere necesarios.
                                </div>






                                @component('components.itemfile')
                                @slot('name','filePhoto')
                                @slot('title','Foto:')
                                @slot('idDiv','divFoto')
                                @slot('slot','')
                                @endcomponent

                                @component('components.itemfile')
                                @slot('name','filePermissionJob')
                                @slot('title','Permiso Trabajo:')
                                @slot('idDiv','divPermiso')
                                @slot('slot','')
                                @endcomponent

                                @component('components.itemfilemultiple')
                                @slot('name','docExtras[]')
                                @slot('title','Documentos Extras:')
                                @slot('idDiv','divDocExtras')
                                @slot('slot','')
                                @endcomponent



                            </div>

                        </div>
                        <div class="panel-footer text-center">
                            <a href="{{route('employees.index')}}" class="btn bg-red waves-effect"><i class="material-icons">keyboard_backspace</i> CANCELAR</a>
                            <button type="submit" class="btn bg-teal waves-effect"><i class="material-icons">save</i> GUARDAR</button>
                        </div>

                    </div>
        {!! Form::close() !!}


    </div>

    @endslot
    @endcomponent

@endsection
@section('cssCustom')
    <link href="{{asset('css/template/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />
@endsection
@section('jsCustom')
    <script src="{{asset('js/template/moment.js')}}" ></script>
    <script src="{{asset('js/template/bootstrap-material-datetimepicker.js')}}" ></script>
    <script src="{{asset('js/employees/employee.js')}}" ></script>


@endsection
