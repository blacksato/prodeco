<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @yield('cssCustom')
    <link href="{{ asset('css/template/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/template/waves.css') }}" rel="stylesheet">
    <link href="{{ asset('css/template/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/template/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/template/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/template/theme-indigo.css') }}" rel="stylesheet">



    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([ 'csrfToken' => csrf_token(),
        ]) !!};
    </script>

</head>
<body class="theme-indigo">
    <div id="app">
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="preloader">
                    <div class="spinner-layer pl-red">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
                <p>Cargando...</p>
            </div>
        </div>
        <!-- #END# Page Loader -->
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <div class="search-bar">
            <div class="search-icon">
                <i class="material-icons">search</i>
            </div>
            @yield('barrabusqueda')
            <div class="close-search">
                <i class="material-icons">close</i>
            </div>
        </div>

        <nav class="navbar">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                    <a href="javascript:void(0);" class="bars"></a>
                    <a class="navbar-brand" href="{{ url('/') }}">  {{ config('app.name', 'Laravel') }}</a>
                </div>
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Call Search -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}"> <i class="material-icons">lock</i></a></li>
                            <li><a href="{{ route('register') }}"> <i class="material-icons">account_circle</i></a></li>
                        @else
                            <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li>
                        <li>

                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="material-icons">exit_to_app</i>
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                          </li>
                        @endif
                        <!-- #END# Call Search -->
 </ul>
                </div>
            </div>
        </nav>

        <section>
            <!-- Left Sidebar -->
            <aside id="leftsidebar" class="sidebar">
                <!-- User Info -->
                <div class="user-info" style="background-color:#201A1A;text-align: center">
                    <div class="image" >
                        <img src="/image/logo.png" alt="User"  width="150px"/>
                    </div>

                </div>
                <!-- #User Info -->
                <!-- Menu -->


                <div class="menu">
                    <ul class="list">
                        <li class="header">OPCIONES</li>

                        <li>
                            <a href="{{url('/home')}}">
                                <i class="material-icons">home</i>
                                <span>Home</span>
                            </a>
                        </li>

                        @if(!Auth::guest())
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="material-icons">exit_to_app</i>  <span>Salir</span>
                                </a>

                            </li>
                            <li>
                                <a href="javascript:void(0);" class="menu-toggle">
                                    <i class="material-icons">supervisor_account</i>
                                    <span>Empleados</span>
                                </a>
                                <ul class="ml-menu">

                                    <li>
                                        <a href="{{ route('employees.index') }}">Lista</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('employees.create') }}">Agregar</a>
                                    </li>


                                </ul>
                            </li>
                            @else

                            <li><a href="{{ route('login') }}"> <i class="material-icons">lock</i> <span>Login</span></a></li>
                            <li><a href="{{ route('register') }}"> <i class="material-icons">account_circle</i> <span>Registro</span></a></li>
                        @endif










                    </ul>

                </div>

                <!-- #Menu -->
                <!-- Footer -->
                <div class="legal">
                    <div class="copyright">
                        &copy; 2917 <a href="javascript:void(0);">Prodeco - SCE</a>.
                    </div>
                    <div class="version">
                        <b>Version: </b> 1.0
                    </div>
                </div>
                <!-- #Footer -->
            </aside>
            <!-- #END# Left Sidebar -->
            <!-- Right Sidebar -->

        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="block-header">
                    <h2>@yield('titleContent','')</h2>
                </div>

                <div class="row clearfix">
                    @include('components.errors')
                    @yield('content')
                </div>
            </div>

        </section>

    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>


    <script src="{{ asset('js/template/waves.js') }}"></script>
    <script src="{{ asset('js/template/admin.js') }}"></script>
    @yield('jsCustom')
    <script type="text/javascript">
        @if (count($errors) > 0)
            $('#modalerrors').modal('show');
        @endif
        $('.alert').not('.important').delay(3000).slideUp(350);
    </script>
</body>
</html>
