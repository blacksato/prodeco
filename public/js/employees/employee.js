/**
 * Created by blacksato on 14/4/2017.
 */
$(function () {
    $('.datepicker').bootstrapMaterialDatePicker({
        format: 'YYYY-MM-DD',
        clearButton: true,
        weekStart: 1,
        time: false
    });
    $("div").removeClass('focused');

    $("select[name=permissionJob]").on('change',function () {
       permissionJob(this.value);
    })

    permissionJob($("select[name=permissionJob]").val());

});
function permissionJob(_value) {
    if(_value=='S'){
        $("#divPermiso").show();
    }else{
        $("#divPermiso").hide();
    }
}